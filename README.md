# Blizz Table Field

## Description

Provides an easy to use table field just to display static tables.

This Module makes use from the JS-library
"handsontable" to have an editable tables in the backend.
It provides formatting options left/right-align, table headers and more.
Inside the table you can not use HTML but
you can use MarkDown what will be rendered as HTML.

## Install

There are two JS-libraries required by this module: Handsontable and PapaParse.
Both will be required by composer with
following configuration in your composer.json.

```
  "repositories": [
    ... ,
    {
      "type": "composer",
      "url": "https://asset-packagist.org"
    }
  ],
  "require": {
    ...
    "oomphinc/composer-installers-extender": "^1.1",
    "drupal/blizz_table_field": "~1.0"
  }
  "extra": {
    "installer-types": [
      "bower-asset"
    ],
    "installer-paths": {
      "docroot/libraries/{$name}": [
        "type:drupal-library",
        "type:bower-asset"
      ],
      ...
    }
```

After install, check the Drupal-status-page
if you can find the libraries in the libraries
folder in your web folder.

## Set up module
1. If you already have a native JSON-field using the
contrib json_field module (any of those JSON fields),
then just set the form widgets and display formatters
to Table widget/formatter.
2. If you wish to create a new field for it,
then add a Table field that this module adds to Drupal.
This field will be saved in "longtext" type, not in
native JSON type in the database. To save the table as
JSON/JSOB type, use the fields provided by the
json_field module.
3. If you used the Table field type, you can save the
table data JSON or CSV format.
4. Optionally you can use MarkDown in the table
cell texts, at page `/admin/config/content/blizz_table_field/settings`
  you can set help text for using MarkDown syntax.
  Here you can also set the license key for
  Handsontable or change the JavaScript library paths.
  In case of a path change, cache clear is required.
