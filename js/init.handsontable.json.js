/**
 * @file
 */

(function (Drupal, drupalSettings) {
    'use strict';
    Drupal.behaviors.handsontablejson = {
        attach: function (context, settings) {

            let renderedTables = document.querySelectorAll('.rendered-table.format-json');

            renderedTables.forEach(function (renderedTable) {
                if (renderedTable.classList.contains('is-loaded')) {
                  return;
                }

                let renderedTableParent = renderedTable.parentElement;
                let textarea = renderedTableParent.querySelector('input[type="hidden"]');
                let textAreaValue = textarea.value;
                let textAreaHtml = textarea.innerHTML;

                if ((textAreaValue.length > 0) && (textAreaHtml === '')) {
                    let config = renderedTable.getAttribute('data-blizz-config');
                    let rows = 1;
                    let columns = 2;
                    let readOnlyRows = 0;
                    let readOnlyColumns = 0;

                    if (config !== null
                        && (config.length > 0)
                    ) {
                        config = JSON.parse(config);
                        rows = config?.rows ?? 1;
                        columns = config?.columns ?? 2;
                        readOnlyRows = config?.readonly_rows ?? 0;
                        readOnlyColumns = config?.readonly_columns ?? 0;
                    }
                    let data = JSON.parse(textAreaValue);
                    let table = new Handsontable(renderedTable, {
                        data: data,
                        cells: function (row, col, prop) {
                            var cellProperties = {};
                            if (config !== null && config[row] !== undefined && config[row][col] !== undefined) {
                                cellProperties.placeholder = config[row][col];
                            }

                            let isRowReadOnly = (readOnlyRows > 0 && (row + 1) <= readOnlyRows);
                            let isColReadOnly = (readOnlyColumns > 0 && (col + 1) <= readOnlyColumns);

                            if (isRowReadOnly || isColReadOnly) {
                              cellProperties.readOnly = true;
                            }

                            return cellProperties;
                        },
                        minRows: rows,
                        minCols: columns,
                        stretchH: 'all',
                        licenseKey: drupalSettings?.handsontable?.license_key ?? "",
                        autoWrapRow: true,
                        contextMenu: Drupal.behaviors.handsontablejson.buildContextMenu(config),
                    });

                    table.updateSettings({
                        afterChange: function (e) {
                          textarea.value = JSON.stringify(table.getData());
                        },
                        afterCreateRow: function (index, amount) {
                          textarea.value = JSON.stringify(table.getData());
                        },
                        afterRemoveCol: function (index, amount) {
                          textarea.value = JSON.stringify(table.getData());
                        },
                        afterRowMove: function (index, amount) {
                          textarea.value = JSON.stringify(table.getData());
                        },
                        afterRemoveRow: function (index, amount) {
                          textarea.value = JSON.stringify(table.getData());
                        },
                        afterCreateCol: function (index, amount) {
                          textarea.value = JSON.stringify(table.getData());
                        },
                        beforeKeyDown: function (e) {
                          // On Tab, go to external button if last cell
                          if (e.keyCode === 9 && table) {
                            let selection = table.getSelected();

                            if (typeof selection !== "undefined" && selection !== null) {
                              let rowIndex = selection[0][0];
                              let colIndex = selection[0][1];

                              if (rowIndex === (table.countRows() - 1) && colIndex === (table.countCols() - 1)) {
                                table.deselectCell();
                                e.stopPropagation();
                              }
                            }

                          }
                        }
                    });

                    renderedTableParent.addEventListener('keyup', function (e) {
                      if (e.keyCode === 9 && !e.shiftKey &&
                        (e.target.classList.contains("handsontableInput") || e.target.classList.contains("blizz")) &&
                        typeof table !== "undefined" &&
                        table !== null && typeof table.getSelected() === "undefined"
                      ) {
                        // Select the first not read-only.
                        let rowCount = table.countRows();
                        let colCount = table.countCols();

                        for (let row = 0; row < rowCount; row++) {
                          for (let col = 0; col < colCount; col++) {
                            let cellMeta = table.getCellMeta(row, col);

                            if (!cellMeta.readOnly) {
                              table.selectCell(row, col);
                              return;
                            }
                          }
                        }

                        // If no editable cells found, use the very first.
                        table.selectCell(0, 0);
                      }
                    });
                }

                renderedTable.classList.add('is-loaded');

            });

        },
        buildContextMenu: function (config) {
          let result = {};
          let limitOperations = config?.limit_operations ?? false;
          let selectedOperations = config?.selected_operations ?? [];

          if (!limitOperations || ("row_above" in selectedOperations)) {
            result.row_above = {
              "name": Drupal.t('Insert row above'),
            };
          }

          if (!limitOperations || ("row_below" in selectedOperations)) {
            result.row_below = {
              "name": Drupal.t('Insert row below'),
            };
            result.hsep1 = "---------";
          }

          if (!limitOperations || ("col_left" in selectedOperations)) {
            result.col_left = {
              "name": Drupal.t('Insert column on the left'),
            };
          }

          if (!limitOperations || ("col_right" in selectedOperations)) {
            result.col_right = {
              "name": Drupal.t('Insert column on the right'),
            };
            result.hsep2 = "---------";
          }

          if (!limitOperations || ("remove_row" in selectedOperations)) {
            result.remove_row = {
              "name": Drupal.t('Remove row'),
            };
          }

          if (!limitOperations || ("remove_col" in selectedOperations)) {
            result.remove_col = {
              "name": Drupal.t('Remove column'),
            };
            result.hsep3 = "---------";
          }

          if (!limitOperations || ("undo" in selectedOperations)) {
            result.undo = {
              "name": Drupal.t('Undo'),
            };
          }

          if (!limitOperations || ("redo" in selectedOperations)) {
            result.redo = {
              "name": Drupal.t('Redo'),
            };
            result.hsep4 = "---------";
          }

          if (!limitOperations || ("cut" in selectedOperations)) {
            result.cut = {
              "name": Drupal.t('Cut'),
            };
          }

          if (!limitOperations || ("copy" in selectedOperations)) {
            result.copy = {
              "name": Drupal.t('Copy'),
            };
          }

          return {
            items: result,
          };

        }
    };

})(Drupal, drupalSettings);
