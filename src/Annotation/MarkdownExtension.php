<?php

namespace Drupal\blizz_table_field\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an markdown extension annotation object.
 *
 * Plugin Namespace: Plugin\MarkdownExtension.
 *
 * @see \Drupal\Core\Archiver\MarkdownExtensionManager
 * @see \Drupal\Core\Archiver\MarkdownExtensionInterface
 * @see plugin_api
 * @see hook_markdown_extension_info_alter()
 *
 * @Annotation
 */
class MarkdownExtension extends Plugin {

  /**
   * The archiver plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the MarkdownExtension plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

  /**
   * The description of the MarkdownExtension plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $description;

}
