<?php

namespace Drupal\blizz_table_field\Event;

/**
 * Defines events where the markdown text can be altered.
 *
 * @package Drupal\blizz_table_field\Event
 */
final class MarkdownEvents {

  const PRE_TEXT_CHANGE = 'text.change.pre';

  const POST_TEXT_CHANGE = 'text.change.post';

  const HELP_TEXT = 'text.change.help';

}
