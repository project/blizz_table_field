<?php

namespace Drupal\blizz_table_field\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event class containing the text to process.
 *
 * @package Drupal\blizz_table_field\Event
 */
class TextToChangeEvent extends Event {

  /**
   * The basic text.
   *
   * @var string
   */
  protected $text;

  /**
   * TextToChangeEvent constructor.
   *
   * @param string $text
   *   The basic text.
   */
  public function __construct($text) {
    $this->text = $text;
  }

  /**
   * Return the text from the event.
   *
   * @return string
   *   The current active text.
   */
  public function getText() {
    return $this->text;
  }

  /**
   * Add text to the event.
   *
   * @param string $text
   *   Add a text.
   */
  public function addText($text) {
    $this->text = $text;
  }

  /**
   * Get latest text.
   *
   * @return string
   *   The current active text.
   */
  public function getLatestText() {
    return $this->text;
  }

}
