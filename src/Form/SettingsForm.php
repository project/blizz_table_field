<?php

namespace Drupal\blizz_table_field\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Created a form with the settings for the module.
 *
 * @package Drupal\blizz_table_field\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'blizz_table_field.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('blizz_table_field.settings');

    $form['license_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('License key'),
      '#description' => $this->t('<a href="@url" target="_blank">More information about licensing.</a>', [
        '@url' => 'https://handsontable.com/docs/license-key/',
      ]),
      '#default_value' => $config->get('license_key'),
    ];

    $form['required_library_files'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Required library files'),
      '#tree' => TRUE,
    ];

    $form['required_library_files']['handsontable_js'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to @file', [
        '@file' => 'handsontable.full.min.js',
      ]),
      '#required' => TRUE,
      '#default_value' => $config->get('required_library_files.handsontable.js') ?? 'libraries/handsontable/handsontable/dist/handsontable.full.min.js',
    ];

    $form['required_library_files']['handsontable_css'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to @file', [
        '@file' => 'handsontable.min.css',
      ]),
      '#required' => TRUE,
      '#default_value' => $config->get('required_library_files.handsontable.css.component') ?? 'libraries/handsontable/handsontable/dist/handsontable.min.css',
    ];

    $form['required_library_files']['papaparse_js'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to @file', [
        '@file' => 'papaparse.min.js',
      ]),
      '#required' => TRUE,
      '#default_value' => $config->get('required_library_files.papaparse.js') ?? 'libraries/papaparse/papaparse.min.js',
    ];

    $form['formatting_options'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Formatting Options'),
      '#default_value' => $config->get('formatting_options'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $handsonTableJs = $form_state->getValue(
      [
        'required_library_files',
        'handsontable_js',
      ]) ?? '';
    $handsonTableCss = $form_state->getValue(
      [
        'required_library_files',
        'handsontable_css',
      ]) ?? '';
    $papaparseJs = $form_state->getValue(
      [
        'required_library_files',
        'papaparse_js',
      ]) ?? '';

    $this->config('blizz_table_field.settings')
      ->set('formatting_options', $form_state->getValue('formatting_options'))
      ->set('license_key', $form_state->getValue('license_key'))
      ->set('required_library_files.handsontable.js', [$handsonTableJs])
      ->set('required_library_files.handsontable.css.component', [$handsonTableCss])
      ->set('required_library_files.papaparse.js', [$papaparseJs])
      ->save();
  }

}
