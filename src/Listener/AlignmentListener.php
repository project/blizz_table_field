<?php

namespace Drupal\blizz_table_field\Listener;

use Drupal\blizz_table_field\Event\MarkdownEvents;
use Drupal\blizz_table_field\Event\TextToChangeEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Sets HTML-alignment based on custom alignment characters.
 *
 * @package Drupal\blizz_table_field\Listener
 */
class AlignmentListener implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      MarkdownEvents::POST_TEXT_CHANGE => [
        ['onTextChangePost', 30],
      ],
    ];
  }

  /**
   * Add class for left or right or center align.
   *
   * @param \Drupal\blizz_table_field\Event\TextToChangeEvent $event
   *   The text to change event.
   */
  public function onTextChangePost(TextToChangeEvent $event) {
    $text = $event->getLatestText();
    // Apply custom alignment (^c for centered & ^r for right-aligned).
    if ((strpos($text, '^c ')) && (strpos($text, '^c ') < 10)) {
      $text = str_replace('^c ', '', $text);
      $text = '<div class="cell-align-center">' . $text . '</div>';
    }
    if ((strpos($text, '^r ')) && (strpos($text, '^r ') < 10)) {
      $text = str_replace('^r ', '', $text);
      $text = '<div class="cell-align-right">' . $text . '</div>';
    }
    if ((strpos($text, '^l ')) && (strpos($text, '^l ') < 10)) {
      $text = str_replace('^l ', '', $text);
      $text = '<div class="cell-align-left">' . $text . '</div>';
    }
    $event->addText($text);
  }

}
