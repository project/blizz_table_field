<?php

namespace Drupal\blizz_table_field\Listener;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\blizz_table_field\Event\MarkdownEvents;
use Drupal\blizz_table_field\Event\TextToChangeEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Calls markdown extensions for further markdown processing.
 *
 * @package Drupal\blizz_table_field\Listener
 */
class ExtendedMarkdown implements EventSubscriberInterface {

  /**
   * The regex to detect markdown.
   */
  const MARKDOWN_REGEX = "/(!)?\\[(.*?)\\]\\((.*?)( +([\\'\"])(.*?)\\5)?\\)/";

  /**
   * The plugin manager.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  private $pluginManager;

  /**
   * ExtendedMarkdown constructor.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $plugin_manager
   *   The plugin manager.
   */
  public function __construct(PluginManagerInterface $plugin_manager) {
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      MarkdownEvents::PRE_TEXT_CHANGE => [
        ['onTextChangePre', 20],
      ],
    ];
  }

  /**
   * Add Markdown extension function.
   *
   * @param \Drupal\blizz_table_field\Event\TextToChangeEvent $event
   *   The text to change event.
   */
  public function onTextChangePre(TextToChangeEvent $event) {

    $extensions = $this->getExtensions();

    $matches = [];
    $cell = $event->getLatestText();

    if (preg_match(self::MARKDOWN_REGEX, $cell, $matches) > 0) {
      $matches_clone = $matches;

      foreach ($matches_clone as $match) {
        $match = str_replace(' ', '', $match);
        foreach ($extensions as $extension) {
          if ($extension->findMatch($match)) {
            $cell = $extension->replaceFiles($cell, $matches);
          }
        }
      }
    }

    $event->addText($cell);
  }

  /**
   * Return for each Plugin Type a Instance.
   *
   * @return \Drupal\blizz_table_field\MarkdownExtensionInterface[]
   *   Returns an Array of MarkdownExtension
   */
  private function getExtensions() {
    static $extension;
    if (!isset($extension)) {
      $extension = [];
      $definitions = $this->pluginManager->getDefinitions();
      foreach ($definitions as $definition) {
        $extension[$definition['id']] = $this->pluginManager->createInstance($definition['id']);
      }
    }

    return $extension;
  }

}
