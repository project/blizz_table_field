<?php

namespace Drupal\blizz_table_field\Listener;

use Drupal\blizz_table_field\Event\MarkdownEvents;
use Drupal\blizz_table_field\Event\TextToChangeEvent;
use Drupal\filter\Plugin\Filter\FilterUrl;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Converts the text in the event into hyperlinks automatically.
 *
 * @package Drupal\blizz_table_field\Listener
 */
class FilterUrlListener implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      MarkdownEvents::POST_TEXT_CHANGE => [
        ['onTextChangePost', 20],
      ],
    ];
  }

  /**
   * Filter Url.
   *
   * @param \Drupal\blizz_table_field\Event\TextToChangeEvent $event
   *   The text to change event.
   */
  public function onTextChangePost(TextToChangeEvent $event) {
    $text = $event->getLatestText();
    // Apply Drupals URL filter.
    $text = _filter_url($text, $this->getUrlFilter());
    $event->addText($text);
  }

  /**
   * Returns a filter object.
   *
   * @return \Drupal\filter\Plugin\Filter\FilterUrl
   *   Returns a FilterUrl object.
   */
  protected function getUrlFilter() {
    $filter_plugin_id = 'filter_url';

    $filter_configuration = [
      'status' => TRUE,
      'settings' => [
        'filter_url_length' => 72,
      ],
      'id' => $filter_plugin_id,
      'type' => 0,
      'class' => 'Drupal\filter\Plugin\Filter\FilterUrl',
      'provider' => 'filter',
    ];

    $filter_plugin_definition = $filter_configuration;
    $filter_plugin_definition['status'] = FALSE;

    return new FilterUrl($filter_configuration, $filter_plugin_id, $filter_plugin_definition);
  }

}
