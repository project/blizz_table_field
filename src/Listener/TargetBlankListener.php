<?php

namespace Drupal\blizz_table_field\Listener;

use Drupal\blizz_table_field\Event\MarkdownEvents;
use Drupal\blizz_table_field\Event\TextToChangeEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Sets the target attribute for the generated HTML links to open in a new tab.
 *
 * @package Drupal\blizz_table_field\Listener
 */
class TargetBlankListener implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      MarkdownEvents::POST_TEXT_CHANGE => [
        ['onTextChangePost', 10],
      ],
    ];
  }

  /**
   * Add target blank to href.
   *
   * @param \Drupal\blizz_table_field\Event\TextToChangeEvent $event
   *   The text to change event.
   */
  public function onTextChangePost(TextToChangeEvent $event) {
    $text = $event->getLatestText();
    // Apply Drupals URL filter.
    $text = str_replace('href=', 'target="_blank" href=', $text);
    $event->addText($text);
  }

}
