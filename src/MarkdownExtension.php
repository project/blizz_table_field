<?php

namespace Drupal\blizz_table_field;

use Drupal\blizz_table_field\Event\MarkdownEvents;
use Drupal\blizz_table_field\Event\TextToChangeEvent;
use League\CommonMark\CommonMarkConverter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Implements methods for creating HTML from markdown.
 *
 * @package Drupal\blizz_table_field
 */
class MarkdownExtension implements MarkdownInterface {

  /**
   * The markdown converter.
   *
   * @var \League\CommonMark\CommonMarkConverter
   */
  private $markdown;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  private $dispatcher;

  /**
   * MarkdownExtension constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The event dispatcher.
   */
  public function __construct(EventDispatcherInterface $dispatcher) {
    $this->markdown = new CommonMarkConverter([
      'html_input' => 'strip',
      'allow_unsafe_links' => FALSE,
    ]);
    $this->dispatcher = $dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function convertToHtml($commonMark) {
    // Error is thrown if we process NULL as string later.
    if (!isset($commonMark)) {
      $commonMark = '';
    }

    $event = new TextToChangeEvent($commonMark);

    // \Symfony\Component\HttpKernel\Kernel::VERSION will give the symfony
    // version. However, testing this does not give the required outcome, we
    // need to test the Drupal core version.
    // @todo Remove the check when Core 9.1 is the lowest supported version.
    if (version_compare(\Drupal::VERSION, '9.1', '>=')) {
      // The new way, with $event first.
      $this->dispatcher->dispatch($event, MarkdownEvents::PRE_TEXT_CHANGE);
    }
    else {
      // Replicate the existing dispatch signature.
      $this->dispatcher->dispatch(MarkdownEvents::PRE_TEXT_CHANGE, $event);
    }

    $event->addText($this->markdown->convert($event->getLatestText()));

    if (version_compare(\Drupal::VERSION, '9.1', '>=')) {
      // The new way, with $event first.
      $this->dispatcher->dispatch($event, MarkdownEvents::POST_TEXT_CHANGE);
    }
    else {
      // Replicate the existing dispatch signature.
      $this->dispatcher->dispatch(MarkdownEvents::POST_TEXT_CHANGE, $event);
    }

    return $event->getLatestText();
  }

}
