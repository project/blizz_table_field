<?php

namespace Drupal\blizz_table_field;

/**
 * Defines methods for processing markdown syntax.
 *
 * @package Drupal\blizz_table_field
 */
interface MarkdownExtensionInterface {

  /**
   * Validate if markdown extension maps.
   *
   * @param string $commomMarkdown
   *   The string which should be tested.
   *
   * @return bool
   *   Return true if found otherwise false.
   */
  public function findMatch($commomMarkdown);

  /**
   * Replace the input string with the markdown extension string.
   *
   * @param string $commonMark
   *   The current active string.
   * @param mixed $matches
   *   All regex matches from markdown.
   *
   * @return string
   *   Return the changed $commonMark string.
   */
  public function replaceFiles($commonMark, $matches);

}
