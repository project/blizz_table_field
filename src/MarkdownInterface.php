<?php

namespace Drupal\blizz_table_field;

/**
 * Defines methods for creating HTML from markdown.
 *
 * @package Drupal\blizz_table_field
 */
interface MarkdownInterface {

  /**
   * Convert a plain text string with markdown to html.
   *
   * @param string $commonMark
   *   The string that should be converted with markdown.
   *
   * @return string
   *   The converted string.
   */
  public function convertToHtml($commonMark);

}
