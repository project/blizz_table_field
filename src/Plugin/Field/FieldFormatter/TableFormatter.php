<?php

namespace Drupal\blizz_table_field\Plugin\Field\FieldFormatter;

use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\blizz_table_field\MarkdownInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'table_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "table_formatter",
 *   label = @Translation("Table formatter"),
 *   field_types = {
 *     "table",
 *     "json",
 *     "json_native",
 *     "json_native_binary"
 *   }
 * )
 */
class TableFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The JsonSerializer.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected $jsonSerializer;

  /**
   * The Markdown Formatter.
   *
   * @var \Drupal\blizz_table_field\MarkdownInterface
   */
  protected $markdown;

  /**
   * Class constructor.
   *
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $settings
   *   Field settings.
   * @param string $label
   *   The label.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Various third party settings.
   * @param \Drupal\Component\Serialization\SerializationInterface $json_serializer
   *   The JSON serialize service.
   * @param \Drupal\blizz_table_field\MarkdownInterface $markdown
   *   The markdown.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, SerializationInterface $json_serializer, MarkdownInterface $markdown) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->jsonSerializer = $json_serializer;
    $this->markdown = $markdown;
  }

  /**
   * Class create method.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container we use for getting the serialization service.
   * @param array $configuration
   *   A configuration array containing various configurations.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   *
   * @return static
   *   Returns something, I don't know what exactly.
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['label'], $configuration['view_mode'], $configuration['third_party_settings'], $container->get('serialization.json'), $container->get('markdown_extension.markdown'));
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'render_header' => TRUE,
      'classes' => '',
      'skip_rendering_markdown' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    $form['render_header'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display with table header'),
      '#default_value' => $this->getSetting('render_header') ?? TRUE,
    ];
    $form['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additional classes'),
      '#default_value' => $this->getSetting('classes') ?? '',
      '#size' => '40',
      '#maxlength' => '64',
    ];
    $form['skip_rendering_markdown'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Skip rendering markdown'),
      '#default_value' => $this->getSetting('skip_rendering_markdown') ?? FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    $summary[] = $this->t('Rendered @header header.', ['@header' => ($this->getSetting('render_header')) ? 'with' : 'without']);

    if (!empty($this->getSetting('classes'))) {
      $summary[] = $this->t('Classes: @classes.', ['@classes' => $this->getSetting('classes')]);
    }

    if (!empty($this->getSetting('skip_rendering_markdown'))) {
      $summary[] = $this->t('Skip rendering markdown');
    }
    else {
      $summary[] = $this->t('Render markdown');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewValue(
        $item,
        $this->fieldDefinition->getItemDefinition()->getSetting('format')
      );
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   * @param string $format
   *   The format.
   *
   * @return array
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item, $format = 'json') {
    switch ($format) {
      case 'csv':
        return $this->tableFromCsv($item);

      default:
        return $this->tableFromJson($item);
    }
  }

  /**
   * Returns a render element of type "table".
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field item.
   *
   * @return array
   *   A render element.
   */
  private function tableFromJson(FieldItemInterface $item) {

    // Get & unserialize data.
    $data = $this->jsonSerializer->decode($item->value);

    $classes = explode(' ', $this->getSetting('classes'));

    $table = [
      '#type' => 'table',
      '#tableselect' => TRUE,
      '#attributes' => [
        'class' => $classes,
      ],
    ];

    $isSkipRenderingMarkdown = (!empty($this->getSetting('skip_rendering_markdown')));

    if ($this->getSetting('render_header')) {
      // Build header from first array item.
      $header = array_values($data[0]);
      unset($data[0]);

      foreach ($header as $key => $val) {
        $header[$key] = [
          'data' => [
            '#markup' => ($isSkipRenderingMarkdown) ?
            $val :
            $this->markdown->convertToHtml($val),
          ],
        ];
      }
      $table['#header'] = $header;
    }
    else {
      $table['#attributes']['class'][] = 'no-header';
    }

    // Iterate over each cell.
    foreach ($data as $row => $content) {
      foreach ($content as $column => $cell) {

        // Transform cell content.
        $cell = ($isSkipRenderingMarkdown) ?
          $cell :
          $this->markdown->convertToHtml($cell);

        $table[$row][$column] = [
          '#markup' => $cell,
        ];
      }
    }

    // Add frontend styling library.
    $table['#attached']['library'][] = 'blizz_table_field/frontend';

    return $table;

  }

  /**
   * Returns a render element of type "table".
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   The field item.
   *
   * @return array
   *   A render element.
   */
  private function tableFromCsv(FieldItemInterface $item) {

    // Get & unserialize data.
    $data = explode("\n", $item->value);

    // Restructure data array.
    $rows = [];
    foreach ($data as $key => $row) {
      $rows[] = str_getcsv($row);
    }

    $classes = explode(' ', $this->getSetting('classes'));

    $table = [
      '#type' => 'table',
      '#tableselect' => TRUE,
      '#attributes' => [
        'class' => $classes,
      ],
    ];

    $isSkipRenderingMarkdown = (!empty($this->getSetting('skip_rendering_markdown')));

    if ($this->getSetting('render_header')) {
      // Build header from first array item.
      $header = array_values($rows[0]);
      unset($rows[0]);

      foreach ($header as $key => $val) {
        $header[$key] = [
          'data' => [
            '#markup' => ($isSkipRenderingMarkdown) ?
            $val :
            $this->markdown->convertToHtml($val),
          ],
        ];
      }
      $table['#header'] = $header;
    }
    else {
      $table['#attributes']['class'][] = 'no-header';
    }

    // Iterate over each cell.
    foreach ($rows as $row => $content) {
      if (is_array($content)) {
        foreach ($content as $column => $cell) {

          // Transform cell content.
          $cell = ($isSkipRenderingMarkdown) ?
            $cell :
            $this->markdown->convertToHtml($cell);

          $table[$row][$column] = [
            '#markup' => $cell,
          ];
        }
      }
    }

    // Add frontend styling library.
    $table['#attached']['library'][] = 'blizz_table_field/frontend';

    return $table;

  }

}
