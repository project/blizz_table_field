<?php

namespace Drupal\blizz_table_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Plugin implementation of the 'table' field type.
 *
 * @FieldType(
 *   id = "table",
 *   label = @Translation("Table"),
 *   description = @Translation("Simple table field."),
 *   default_widget = "table_widget",
 *   default_formatter = "table_formatter"
 * )
 */
class Table extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'format' => 'json',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Prevent early t() calls by using the TranslationWrapper.
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Text value'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'value' => [
          'type' => 'text',
          'size' => 'big',
          'format' => $field_definition->getSetting('format'),
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    // Gets the field definition's default value literal.
    $values = $field_definition->getDefaultValueLiteral();
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = [];

    $format_options = [
      'json' => $this->t('JSON'),
      'csv' => $this->t('CSV'),
    ];

    $elements['format'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $format_options,
      '#title' => $this->t('Format'),
      '#default_value' => $this->getSetting('format'),
      '#empty_value' => '',
      '#description' => $this->t('The format the table data is stored as.'),
      '#disabled' => $has_data,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

}
