<?php

namespace Drupal\blizz_table_field\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'table_widget' widget.
 *
 * @FieldWidget(
 *   id = "table_widget",
 *   label = @Translation("Table widget"),
 *   field_types = {
 *     "table",
 *     "json",
 *     "json_native",
 *     "json_native_binary"
 *   }
 * )
 */
class TableWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * The configuration object for this widget.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('config.factory')
    );
  }

  /**
   * TableWidget constructor.
   *
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition
   *   The plugin definition array.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition object.
   * @param array $settings
   *   The settings array.
   * @param array $third_party_settings
   *   The third party settings array.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal's config factory service.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->config = $config_factory->get('blizz_table_field.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'size' => 60,
      'rows' => 1,
      'columns' => 2,
      'readonly_rows' => 0,
      'readonly_columns' => 0,
      'limit_operations' => FALSE,
      'enabled_operations' => [],
      'hide_formatting_options' => FALSE,
      'placeholder' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['size'] = [
      '#type' => 'number',
      '#title' => $this->t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    ];

    $elements['rows'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimal rows'),
      '#default_value' => $this->getSetting('rows') ?? 1,
      '#min' => 1,
    ];

    $elements['columns'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimal columns'),
      '#default_value' => $this->getSetting('columns') ?? 2,
      '#min' => 1,
    ];

    $elements['readonly_rows'] = [
      '#type' => 'number',
      '#title' => $this->t('Read-only rows'),
      '#default_value' => $this->getSetting('readonly_rows') ?? 0,
      '#min' => 0,
    ];

    $elements['readonly_columns'] = [
      '#type' => 'number',
      '#title' => $this->t('Read-only columns'),
      '#default_value' => $this->getSetting('readonly_columns') ?? 0,
      '#min' => 0,
    ];

    $elements['limit_operations'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Limit operations'),
      '#default_value' => $this->getSetting('limit_operations') ?? FALSE,
      '#attributes' => [
        'data-name' => [
          'limit_operations',
        ],
      ],
    ];

    $elements['enabled_operations'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled operations'),
      '#default_value' => $this->getSetting('enabled_operations') ?? [],
      '#options' => $this->getOperationOptions(),
      '#states' => [
        'visible' => [
          ':input[data-name="limit_operations"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $elements['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    $elements['hide_formatting_options'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Hide formatting options'),
      '#default_value' => $this->getSetting('hide_formatting_options'),
      '#description' => $this->t('If the markup is not rendered for this table later, the Formatting options dropdown help can be hidden by checking this option.'),

    ];

    return $elements;
  }

  /**
   * Returns operations for checkbox element.
   *
   * @return string[]
   *   Operation ID as key, translatable markup as value.
   */
  protected function getOperationOptions() {
    return [
      'row_above' => $this->t('Insert row above'),
      'row_below' => $this->t('Insert row below'),
      'col_left' => $this->t('Insert column on the left'),
      'col_right' => $this->t('Insert column on the right'),
      'remove_row' => $this->t('Remove row'),
      'remove_col' => $this->t('Remove column'),
      'undo' => $this->t('Undo'),
      'redo' => $this->t('Redo'),
      'cut' => $this->t('Cut'),
      'copy' => $this->t('Copy'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $placeholder_setting = $this->getSetting('placeholder');
    $summary[] = $this->t('Textfield size: @size', ['@size' => $this->getSetting('size')]);
    if (!empty($placeholder_setting)) {
      $summary[] = $this->t('Placeholder: @placeholder', ['@placeholder' => $placeholder_setting]);
    }

    $summary[] = $this->t('Minimal rows: @placeholder', ['@placeholder' => $this->getSetting('rows') ?? 1]);
    $summary[] = $this->t('Minimal columns: @placeholder', ['@placeholder' => $this->getSetting('columns') ?? 2]);
    $summary[] = $this->t('Read-only rows: @placeholder', ['@placeholder' => $this->getSetting('readonly_rows') ?? 0]);
    $summary[] = $this->t('Read-only columns: @placeholder', ['@placeholder' => $this->getSetting('readonly_columns') ?? 0]);
    $isLimitOperations = !empty($this->getSetting('limit_operations'));
    if ($isLimitOperations) {
      $allOperations = $this->getOperationOptions();
      $selectedOperations = $this->getSetting('enabled_operations') ?? [];
      $selectedOperations = array_filter($selectedOperations);

      $result = [];

      foreach ($selectedOperations as $selectedOperation) {
        if (isset($allOperations[$selectedOperation])) {
          $result[] = $allOperations[$selectedOperation];
        }
      }

      $resultString = implode(', ', $result);

      $summary[] = $this->t('Operations are limited to: @placeholder', ['@placeholder' => $resultString]);
    }
    else {
      $summary[] = $this->t('All operations are enabled.');
    }

    $summary[] = (empty($this->getSetting('hide_formatting_options'))) ?
      $this->t('Hide formatting options') :
      $this->t('Show formatting options');

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $title = ($element['#title']) ?? $this->t('Table data');

    $format = $items->getSetting('format');

    // If the format is not set in the field settings, we assume that it is
    // a field from the json_field module.
    if (empty($format)) {
      $format = 'json';
    }
    else {
      $format = Xss::filter($format);
    }

    $default['json'] = "[\r\n[\r\n\"Column 1\",\r\n\"Column 2\",\r\n\"Column 3\",\r\n\"Column 4\"\r\n],\r\n[\r\n\"\",\r\n\"\",\r\n\"\",\r\n\"\"\r\n],\r\n[\r\n\"\",\r\n\"\",\r\n\"\",\r\n\"\"\r\n],\r\n[\r\n\"\",\r\n\"\",\r\n\"\",\r\n\"\"\r\n],\r\n[\r\n\"\",\r\n\"\",\r\n\"\",\r\n\"\"\r\n ]\r\n]";
    $default['csv'] = " Column 1, Column 2, Column 3, Column 4 \r\n,,,\r\n,,,\r\n,,,\r\n,,,";

    $element['value'] = $element + [
      '#type' => 'hidden',
      '#description' => $this->t('The serialized table data.'),
      '#size' => $this->getSetting('size'),
      '#placeholder' => $this->getSetting('placeholder'),
      '#weight' => -15,
    ];

    $element['value']['#attached']['library'] = ['blizz_table_field/handsontable-' . $format];
    $element['value']['#default_value'] = (isset($items[$delta]->value)) ? $items[$delta]->value : $default[$format];

    $element['pseudo'] = [
      '#type' => 'item',
      '#title' => $title,
    ];

    $rows = $this->getSetting('rows') ?? 1;
    $columns = $this->getSetting('columns') ?? 2;

    $readOnlyRows = $this->getSetting('readonly_rows') ?? 0;
    $readOnlyColumns = $this->getSetting('readonly_columns') ?? 0;

    $limitOperations = $this->getSetting('limit_operations') ?? FALSE;
    $selectedOperations = $this->getSetting('enabled_operations') ?? [];
    $selectedOperations = array_filter($selectedOperations);

    $config = [
      'rows' => (int) $rows,
      'columns' => (int) $columns,
      'readonly_rows' => (int) $readOnlyRows,
      'readonly_columns' => (int) $readOnlyColumns,
      'limit_operations' => $limitOperations,
      'selected_operations' => $selectedOperations,
    ];

    $element['table'] = [
      '#type' => 'inline_template',
      '#template' => '<div class="rendered-table blizz format-{{ format }}" data-blizz-config="{{ config }}" tabIndex="0"></div>',
      '#context' => [
        'format' => $format,
        'config' => json_encode((object) $config),
      ],
      '#weight' => 15,
    ];

    if (empty($this->getSetting('hide_formatting_options'))) {
      $description = $this->config->get('formatting_options');

      if (!empty($description)) {
        $element['description'] = [
          '#type' => 'details',
          '#title' => $this->t('Formatting options'),
          '#description' => $description,
          '#weight' => 20,
        ];
      }
    }

    $licenseKey = $this->config->get('license_key') ?? '';
    $element['value']['#attached']['drupalSettings']['handsontable']['license_key'] = $licenseKey;

    return $element;
  }

}
