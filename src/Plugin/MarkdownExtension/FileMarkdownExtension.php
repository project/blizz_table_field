<?php

namespace Drupal\blizz_table_field\Plugin\MarkdownExtension;

use Drupal\blizz_table_field\MarkdownExtensionInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Defines an MarkdownExtension implementation for files.
 *
 * @MarkdownExtension(
 *   id = "file_markdown_extension",
 *   title = @Translation("FileMarkdownExtension"),
 *   description = @Translation("Allow File Markdown Extension.")
 * )
 */
class FileMarkdownExtension implements MarkdownExtensionInterface {

  const MARKDOWN_REGEX = "/\[.*?\]\(([0-9]*?)\)/";

  /**
   * {@inheritdoc}
   */
  public function findMatch($commomMarkdown) {
    // Make sure we only apply the file url generation to images and not links.
    $other_matches = [];
    if (preg_match_all(self::MARKDOWN_REGEX, $commomMarkdown, $other_matches) > 0) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function replaceFiles($commonMark, $matches) {
    // Get media entity ID.
    $file_definition = array_pop($matches);
    $image = explode(',', $file_definition);

    // Get image url by media entity ID & optional view mode.
    $image = $this->fileLink($image[0]);
    if ($image) {
      // Replace image definition with image url.
      $commonMark = str_replace($file_definition, $image, $commonMark);
    }
    return $commonMark;
  }

  /**
   * Create a link from a media id.
   *
   * @param int $media_entity_id
   *   The media entity id which should be linked.
   *
   * @return bool|string
   *   Return false if fileLink didn't work or a link to media entity.
   */
  protected function fileLink($media_entity_id) {
    $rendered = FALSE;

    // Make sure we're dealing with a numeric entity ID.
    if (is_numeric($media_entity_id)) {

      // Use EntityManager.
      $entity_manager = \Drupal::service('entity_type.manager');

      // Load media entity by ID.
      if ($entity = $entity_manager->getStorage('media')
        ->load($media_entity_id)
      ) {

        if (!$entity->hasField('field_image_file')) {
          if ($entity->hasField('field_file')) {
            if ($file_id = $entity->get('field_file')->target_id) {
              if ($file = File::load($file_id)) {
                // Get Image URI & load Image.
                $uri = $file->getFileUri();
                $url = Url::fromUri(\Drupal::service('file_url_generator')->generateAbsoluteString($uri));
                $rendered = $url->getUri();
              }
            }
          }
        }
        return $rendered;
      }
    }

    return $rendered;
  }

}
