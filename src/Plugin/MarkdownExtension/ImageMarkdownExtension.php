<?php

namespace Drupal\blizz_table_field\Plugin\MarkdownExtension;

use Drupal\blizz_table_field\MarkdownExtensionInterface;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;

/**
 * Defines an MarkdownExtension implementation for files.
 *
 * @MarkdownExtension(
 *   id = "image_markdown_extension",
 *   title = @Translation("ImageMarkdownExtension"),
 *   description = @Translation("Allow Image Markdown Extension.")
 * )
 */
class ImageMarkdownExtension implements MarkdownExtensionInterface {

  /**
   * {@inheritdoc}
   */
  public function findMatch($commomMarkdown) {

    // Make sure we only apply the image url generation to images and not links.
    if (strpos($commomMarkdown, '!') !== FALSE) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function replaceFiles($commonMark, $matches) {
    // Get media entity ID & optional view mode.
    $image_definition = array_pop($matches);

    try {
      $image = $this->splitTag(',', $image_definition);

      // Get image url by media entity ID & optional view mode.
      $image = $this->imageUrl($image[0], $image[1]);
      if ($image) {
        // Replace image definition with image url.
        $commonMark = str_replace($image_definition, $image, $commonMark);
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('blizz_table_field')->error("Wrong usage of markdown tag in node:");
    }

    return $commonMark;
  }

  /**
   * Explode a tag or throw a exception.
   *
   * @param string $tag
   *   The cut string.
   * @param string $image_definition
   *   The image definition string.
   *
   * @return array
   *   The exploded array.
   *
   * @throws \Exception
   */
  public function splitTag($tag, $image_definition) {
    $values = explode($tag, $image_definition);
    if (isset($values[0]) && isset($values[1])) {
      return $values;
    }
    throw new \Exception('ImageMarkdown failed because you use a wrong syntax');
  }

  /**
   * Returns an image url.
   *
   * @param int $media_entity_id
   *   The media entity id.
   * @param string $image_style
   *   The optional image style, as machine name.
   *
   * @return string
   *   An image url.
   */
  protected function imageUrl($media_entity_id, $image_style = 'thumbnail') {

    // Eliminate any leading or trailing slashes.
    $image_style = str_replace(' ', '', $image_style);

    $rendered = FALSE;

    // Make sure we're dealing with a numeric entity ID.
    if (is_numeric($media_entity_id)) {

      // Use EntityManager.
      $entity_manager = \Drupal::service('entity_type.manager');

      /** @var \Drupal\media\MediaInterface $entity */
      if ($entity = $entity_manager->getStorage('media')
        ->load($media_entity_id)
      ) {
        // Load image entity by reference.
        if ($file_id = $entity->get('field_image_file')->target_id) {

          /** @var \Drupal\file\Entity\File $file */
          if ($file = File::load($file_id)) {

            // Get Image URI & load Image.
            $uri = $file->getFileUri();

            $image = \Drupal::service('image.factory')->get($uri);

            // Check if entity is of Image.
            if ($image->isValid()) {

              // Load available image style derivative of image or
              // default to thumbnail derivative.
              if ($image_style_entity = ImageStyle::load($image_style)) {
                $image_url = $image_style_entity->buildUrl($file->getFileUri());
              }
              else {
                $image_style_entity = ImageStyle::load('thumbnail');
                $image_url = $image_style_entity->buildUrl($uri);
              }
              $rendered = $image_url;
            }
          }
        }
      }
    }
    return $rendered;
  }

}
