<?php

namespace Drupal\Tests\blizz_table_field\Plugin\MarkdownExtension;

use Drupal\blizz_table_field\Plugin\MarkdownExtension\FileMarkdownExtension;

/**
 * Contains tests for MarkdownExtension implementation for files.
 *
 * @package Drupal\Tests\blizz_table_field\Plugin\MarkdownExtension
 */
class FileMarkdownExtensionTest extends \PHPUnit_Framework_TestCase {

  /**
   * MarkdownExtension implementation for files.
   *
   * @var \Drupal\blizz_table_field\Plugin\MarkdownExtension\FileMarkdownExtension
   */
  protected $fileMarkdownExtension;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    $this->fileMarkdownExtension = new FileMarkdownExtension();
  }

}
